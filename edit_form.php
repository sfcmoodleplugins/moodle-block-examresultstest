<?php
 
class block_examresultstest_edit_form extends block_edit_form {
 
    protected function specific_definition($mform) {
 
        // Section header title according to language file.
        $mform->addElement('header', 'configheader', get_string('blocksettings', 'block'));
 
        // A sample string variable with a default value.
        $mform->addElement('text', 'config_color', get_string('blockcolor', 'block_examresultstest'));
        $mform->setDefault('config_color', '#ad0000');
        $mform->setType('config_color', PARAM_MULTILANG);        
 
    }
}