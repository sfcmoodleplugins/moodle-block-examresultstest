<?php

defined('MOODLE_INTERNAL') || die;


$settings->add(new admin_setting_configtext(
            'examresultstest/Enable_Date',
            get_string('labelenabledate', 'block_examresultstest'),
            date('d-m-Y h:i:s',get_config('examresultstest', 'Enable_Date')) . get_string('descenabledate', 'block_examresultstest'),
            '0'
        ));
        
$settings->add(new admin_setting_configtext(
            'examresultstest/Disable_Date',
            get_string('labeldisabledate', 'block_examresultstest'),
            date('d-m-Y h:i:s',get_config('examresultstest', 'Disable_Date')) . get_string('descdisabledate', 'block_examresultstest'),
            '0'
        ));

$settings->add(new admin_setting_configtext(
            'examresultstest/dbserver',
            get_string('labeldbserver', 'block_examresultstest'),
            get_string('descdbserver', 'block_examresultstest'),
            ''
        ));
$settings->add(new admin_setting_configtext(
            'examresultstest/dbport',
            get_string('labeldbport', 'block_examresultstest'),
            get_string('descdbport', 'block_examresultstest'),
            ''
        ));
$settings->add(new admin_setting_configtext(
            'examresultstest/dbname',
            get_string('labeldbname', 'block_examresultstest'),
            get_string('descdbname', 'block_examresultstest'),
            ''
        ));
$settings->add(new admin_setting_configtext(
            'examresultstest/dbuser',
            get_string('labeldbuser', 'block_examresultstest'),
            get_string('descdbuser', 'block_examresultstest'),
            ''
        ));
$settings->add(new admin_setting_configpasswordunmask(
            'examresultstest/dbpassword',
            get_string('labeldbpassword', 'block_examresultstest'),
            get_string('descdbpassword', 'block_examresultstest'),
            ''
        ));
$settings->add(new admin_setting_configtext(
            'examresultstest/dbquery',
            get_string('labeldbquery', 'block_examresultstest'),
            get_string('descdbquery', 'block_examresultstest'),
            ''
        ));