<?php
class block_examresultstest extends block_base {
    public function init() {
        $this->title = get_string('examresultstest', 'block_examresultstest');
    }
    
    function has_config() {
        return true;
    }

    
    // The PHP tag and the curly bracket for the class definition 
    // will only be closed after there is another function added in the next section.
    public function get_content() {
    global $CFG, $USER, $COURSE, $SITE;
    if ($this->content !== null) {
      return $this->content;
    }
    $textcolor = "#ab0000";
    if (! empty($this->config->color)) {
      $textcolor = $this->config->color;
    }
 
    $this->content         =  new stdClass;
    $urlgrades  = "{$CFG->wwwroot}/blocks/examresultstest/view_grades.php";
    $this->content->text   = "<a href='{$urlgrades}'><span style='color: " . $textcolor . ";'>Exam Results</span></a></br>";
 
    return $this->content;
  }

public function hide_header() {
  return true;
}
}   // Here's the closing bracket for the class definition

