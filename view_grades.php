<?php
global $DB, $OUTPUT, $PAGE;

require_once('../../config.php');
//require_once($CFG->dirroot.'/blocks/examresultstest/show_array.php');
 
// Check for all required variables.
 
require_login();
$PAGE->set_context(context_system::instance());
$PAGE->set_url('/blocks/examresultstest/view_grades.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_heading(get_string('myexamresults', 'block_examresultstest'));

echo $OUTPUT->header();

$enableDate = get_config('examresultstest', 'Enable_Date');
$disableDate = get_config('examresultstest', 'Disable_Date');
$time = time();

if ( $time >= $enableDate AND $time <= $disableDate ) {

  $examresults = $DB->get_records('block_examresultstest', array('studentid' => $USER->idnumber, 'examtype' => 'Syllabus'));
  //echo $USER->idnumber;
  //echo print_r($examresults);
  $x=0;
  echo "<table cellspacing='0' style='border: 2px solid white;'>";
  echo "<tr style='BACKGROUND-COLOR: #DEDEDE; border: 2px solid white;'>";
  //echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white; text-align: center; white-space: nowrap; font-weight:bold;'>Student ID</td>";
  echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white; text-align: center; white-space: nowrap; font-weight:bold;'>Cand no</td>";
  echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white; text-align: center; white-space: nowrap; font-weight:bold;'>Exam Board</td>";
  echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white; text-align: center; white-space: nowrap; font-weight:bold;'>Subject Ref</td>";
  echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white; text-align: center; white-space: nowrap; font-weight:bold;'>Subject Name</td>";
  echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white; text-align: center; white-space: nowrap; font-weight:bold;'>Level</td>";
  echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white; text-align: center; white-space: nowrap; font-weight:bold;'>Series</td>";
  echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white; text-align: center; white-space: nowrap; font-weight:bold;'>Year</td>";
  echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white; text-align: center; white-space: nowrap; font-weight:bold;'>Grade</td>";
  echo "</tr>";
  foreach ($examresults as $result) {
    $x=$x+1;
    if($x&1) {
    //odd line
      echo "<tr>";
    }  else {
    //even line
      echo "<tr style='BACKGROUND-COLOR: #efe7e7;'>";
    }
    //echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white;'>".$result->studentid."</td>";
    echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white;'>".$result->candno."</td>";
    echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white;'>".$result->examboard."</td>";
    echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white;'>".$result->subjectref."</td>";
    echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white;'>".$result->subjectname."</td>";
    echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white;'>".$result->examlevel."</td>";
    echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white;'>".$result->seriescode."</td>";
    echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white;'>".$result->year."</td>";
    echo "<td style='border-width: 1px; padding: 1px; border: 2px solid white;'>".$result->grade."</td>";
    echo "</tr>";
  }
  echo "</table>";

}
else {
 if ( $time >= $disableDate ) {
    echo "Exam Results are currently unavailable";
  }
  else {
   echo "Exam Results will be released at " . date('g:ia \o\n l jS F Y',$enableDate);
  }
}

echo $OUTPUT->footer();