<?php
    $string['pluginname'] = 'Exam Results Test block';
    $string['examresultstest'] = 'Exam Results';
    $string['myexamresults'] = 'My Exam Results';
    $string['blockcolor'] = 'Text Colour';
    $string['importexamresults'] = 'Import Exam Results from UNIT-e';

    $string['labelenabledate'] = 'Enable Results Page on: ';
    $string['descenabledate'] = '<br />Unix timestamp to Enable the display of the Exam Results Page - use the <a href=http://www.onlineconversion.com/unix_time.htm><b>http://www.onlineconversion.com/unix_time.htm</b></a> to create the Unix timestamp';
    $string['labeldisabledate'] = 'Disable Results Page on: ';
    $string['descdisabledate'] = '<br />Unix timestamp to Disable the display of the Exam Results Page - use the <a href=http://www.onlineconversion.com/unix_time.htm><b>http://www.onlineconversion.com/unix_time.htm</b></a> to create the Unix timestamp';


    $string['labeldbserver'] = 'Database Server:';
    $string['descdbserver'] = 'Must be the format of DNSName\InstanceName';
    $string['labeldbport'] = 'Database Port:';
    $string['descdbport'] = 'SQL server port';
    $string['labeldbname'] = 'Database Name:';
    $string['descdbname'] = 'SQL Database Name';
    $string['labeldbuser'] = 'Database User:';
    $string['descdbuser'] = 'Username to connect to database';
    $string['labeldbpassword'] = 'Database User Password:';
    $string['descdbpassword'] = 'Password for Database User';
    $string['labeldbquery'] = 'Database Exam Query:';
    $string['descdbquery'] = 'Exam Query for Database';