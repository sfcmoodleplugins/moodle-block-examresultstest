<?php
    $plugin->version = 2018082814;  // YYYYMMDDHH (year, month, day, 24-hr time)
    $plugin->requires = 2010112400; // YYYYMMDDHH (This is the release version for Moodle 2.0)
    $plugin->component = 'block_examresultstest'; // Declare the type and name of this plugin.