<?php

defined('MOODLE_INTERNAL') || die();

$tasks = array( 
    array(
        'classname' => 'block_examresultstest\task\import_exam_results',
        'blocking' => 0,
        'minute' => '00',
        'hour' => '07',
        'day' => '*', 
        'dayofweek' => '4', 
        'month' => '*'
    )
);