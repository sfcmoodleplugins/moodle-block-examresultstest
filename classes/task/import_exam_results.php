<?php
namespace block_examresultstest\task;

class import_exam_results extends \core\task\scheduled_task {      
    public function get_name() {
        // Shown in admin screens
        return get_string('importexamresults', 'block_examresultstest');
    }

    public function execute() {
        $timenow = time();

        set_time_limit(3600); // 1 hour should be enough
        raise_memory_limit(MEMORY_HUGE);

        $dbserver = get_config('examresultstest', 'dbserver');
        $dbport = get_config('examresultstest', 'dbport');
        $dbname = get_config('examresultstest', 'dbname');
        $dbuser = get_config('examresultstest', 'dbuser');
        $dbpassword = get_config('examresultstest', 'dbpassword');
        $sqlViewName = get_config('examresultstest', 'dbquery');

        global $DB;
        $DB->delete_records('block_examresultstest');

        $serverName = "$dbserver, $dbport"; //serverName\instanceName, portNumber (1433 by default)
        $connectionInfo = array( "Database"=>"$dbname", "UID"=>"$dbuser", "PWD"=>"$dbpassword");

        $conn = sqlsrv_connect( $serverName, $connectionInfo);

        if( $conn === false) {
            die( print_r( sqlsrv_errors(), true));
        }
        
        // Set up the proc params array - be sure to pass the param by reference
        $procedure_params = array(
            array($sqlViewName, SQLSRV_PARAM_IN),
        );
        $sql = "declare @sql as nvarchar(MAX); set @sql = dbo.GetLiteViewSQL(?); exec sp_executesql @sql";
        $stmt = sqlsrv_prepare($conn, $sql, $procedure_params);
        
        if( !$stmt ) {
            die( print_r( sqlsrv_errors(), true));
        }
        if( sqlsrv_execute( $stmt ) === false ) {
            die( print_r( sqlsrv_errors(), true));
        }

        while( $r = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
            $row = new \stdClass();
            foreach($r as $k=>$v) {
                $row->$k = $v;
            }
            $row->id = $DB->insert_record('block_examresultstest', $row);
            unset($row);
        }

        sqlsrv_free_stmt($stmt);

        return true;

    }
} 